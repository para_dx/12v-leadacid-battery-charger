### Lead-Acid Battery charger for 12 V Pb Batteries 

This charger is designed to fit in a DIN-Rail housing.

I build this for the use on a sailing vessle and to be charged by a 15V DC PSU when connected to shore power.

There is a simple Diode circuit to automatically feed the onboard electronics from external power and start charging the batteries.


