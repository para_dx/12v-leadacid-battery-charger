EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 61F8DEE5
P 5750 2900
F 0 "J3" H 5850 2950 50  0000 L CNN
F 1 "VOut" H 5850 2850 50  0000 L CNN
F 2 "Connector_Molex:Molex_Nano-Fit_105313-xx02_1x02_P2.50mm_Horizontal" H 5750 2900 50  0001 C CNN
F 3 "~" H 5750 2900 50  0001 C CNN
	1    5750 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 61F4DB43
P 4850 2900
F 0 "J2" H 4950 2950 50  0000 L CNN
F 1 "VBatt" H 4950 2850 50  0000 L CNN
F 2 "Connector_Molex:Molex_Nano-Fit_105313-xx02_1x02_P2.50mm_Horizontal" H 4850 2900 50  0001 C CNN
F 3 "~" H 4850 2900 50  0001 C CNN
	1    4850 2900
	1    0    0    -1  
$EndComp
Text GLabel 2300 1750 1    50   Input ~ 0
V15
Text GLabel 5550 1750 1    50   Input ~ 0
VOut
$Comp
L MySymbols:SF300XPT D1
U 1 1 61F3BA06
P 5000 2100
F 0 "D1" H 5000 2425 50  0000 C CNN
F 1 "SF300XPT" H 5000 2334 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-247-3_Vertical" H 5000 2350 50  0001 C CNN
F 3 "https://www.mouser.de/datasheet/2/395/SF3001PT_SERIES_J2103-2821789.pdf" H 5000 2350 50  0001 C CNN
	1    5000 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 61F29CF5
P 3500 2400
F 0 "R3" V 3304 2400 50  0000 C CNN
F 1 "0.2" V 3395 2400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0614_L14.3mm_D5.7mm_P20.32mm_Horizontal" H 3500 2400 50  0001 C CNN
F 3 "~" H 3500 2400 50  0001 C CNN
	1    3500 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 61F069FA
P 3700 2950
F 0 "R2" H 3750 3050 50  0000 L CNN
F 1 "2.4k" H 3750 2950 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" H 3700 2950 50  0001 C CNN
F 3 "~" H 3700 2950 50  0001 C CNN
	1    3700 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 61EE1893
P 3700 2600
F 0 "R1" H 3759 2646 50  0000 L CNN
F 1 "240" H 3759 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" H 3700 2600 50  0001 C CNN
F 3 "~" H 3700 2600 50  0001 C CNN
	1    3700 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 61F4EDCF
P 1600 2900
F 0 "J1" H 1800 2900 50  0000 C CNN
F 1 "VIn" H 1800 3050 50  0000 C CNN
F 2 "Connector_Molex:Molex_Nano-Fit_105313-xx02_1x02_P2.50mm_Horizontal" H 1600 2900 50  0001 C CNN
F 3 "~" H 1600 2900 50  0001 C CNN
	1    1600 2900
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 61EDBBAC
P 3250 3600
F 0 "#PWR0101" H 3250 3350 50  0001 C CNN
F 1 "GND" H 3255 3427 50  0000 C CNN
F 2 "" H 3250 3600 50  0001 C CNN
F 3 "" H 3250 3600 50  0001 C CNN
	1    3250 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 61F32D40
P 3250 2950
F 0 "C2" H 3342 2996 50  0000 L CNN
F 1 "100nF" H 3342 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 3250 2950 50  0001 C CNN
F 3 "~" H 3250 2950 50  0001 C CNN
	1    3250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3450 3250 3450
Connection ~ 3250 3450
Wire Wire Line
	3250 3450 3700 3450
Wire Wire Line
	3700 3450 3700 3050
Wire Wire Line
	4300 3100 4300 3450
Connection ~ 3700 3450
Wire Wire Line
	4650 3000 4650 3450
Wire Wire Line
	3700 3450 4300 3450
Connection ~ 4300 3450
Wire Wire Line
	4300 3450 4650 3450
Wire Wire Line
	4300 2800 4300 2200
Connection ~ 4300 2200
Wire Wire Line
	4300 2200 3700 2200
Wire Wire Line
	3700 2200 3700 2400
Wire Wire Line
	3600 2400 3700 2400
Connection ~ 3700 2400
Wire Wire Line
	3700 2400 3700 2500
$Comp
L Regulator_Linear:LM317_TO3 U1
U 1 1 61EB442A
P 3000 2400
F 0 "U1" H 3000 2642 50  0000 C CNN
F 1 "LM317_TO3" H 3000 2551 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-3" H 3000 2600 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm317.pdf" H 3000 2400 50  0001 C CNN
	1    3000 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2000 2300 2400
Wire Wire Line
	3300 2400 3400 2400
Connection ~ 2300 2000
Connection ~ 2300 3450
Wire Wire Line
	2300 3450 2650 3450
Connection ~ 2650 3450
Wire Wire Line
	2650 3050 2650 3450
Wire Wire Line
	3700 2700 3700 2800
Wire Wire Line
	3000 2700 3000 2800
Wire Wire Line
	3000 2800 3250 2800
Connection ~ 3700 2800
Wire Wire Line
	3700 2800 3700 2850
Wire Wire Line
	3250 2800 3250 2850
Connection ~ 3250 2800
Wire Wire Line
	3250 2800 3700 2800
Wire Wire Line
	3250 3050 3250 3450
Wire Wire Line
	3250 3450 3250 3600
$Comp
L Device:C_Small C1
U 1 1 61EE0A6F
P 2650 2950
F 0 "C1" H 2742 2996 50  0000 L CNN
F 1 "100nF" H 2742 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 2650 2950 50  0001 C CNN
F 3 "~" H 2650 2950 50  0001 C CNN
	1    2650 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 620DFE7C
P 4300 2950
F 0 "C4" H 4450 3050 50  0000 L CNN
F 1 "10uF" H 4450 2950 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.3" H 4418 2859 50  0001 L CNN
F 3 "~" H 4300 2950 50  0001 C CNN
	1    4300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3100 2300 3450
$Comp
L Device:CP C3
U 1 1 620FD4B5
P 2300 2950
F 0 "C3" H 2418 2996 50  0000 L CNN
F 1 "10uF" H 2418 2905 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.3" H 2338 2800 50  0001 C CNN
F 3 "~" H 2300 2950 50  0001 C CNN
	1    2300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1750 5550 2100
Wire Wire Line
	5250 2100 5550 2100
Connection ~ 5550 2100
Wire Wire Line
	4750 2000 2300 2000
Wire Wire Line
	2650 2850 2650 2400
Wire Wire Line
	2650 2400 2700 2400
Wire Wire Line
	5550 3000 5550 3450
Wire Wire Line
	5550 3450 4650 3450
Connection ~ 4650 3450
Wire Wire Line
	2650 2400 2300 2400
Connection ~ 2650 2400
Connection ~ 2300 2400
Wire Wire Line
	1800 3450 2300 3450
Wire Wire Line
	1800 3000 1800 3450
Wire Wire Line
	2300 2400 2300 2800
Wire Wire Line
	2300 2000 1800 2000
Wire Wire Line
	2300 1750 2300 2000
Text Notes 4750 11050 0    55   ~ 0
16.02.2022
Text Notes 3950 10950 0    79   ~ 0
Schematic Lead Acid Battery Charger
Text Notes 3600 10100 0    55   ~ 0
Lead Acid Battery Charger \w 15V input and up to 14,6V output
Text Notes 3600 10250 0    55   ~ 0
Max charge current up to 2.2 A \n
Wire Wire Line
	4300 2200 4650 2200
Connection ~ 4650 2200
Wire Wire Line
	4650 2200 4750 2200
Wire Wire Line
	5550 2100 5550 2900
Wire Wire Line
	4650 2200 4650 2900
Wire Wire Line
	1800 2000 1800 2900
$EndSCHEMATC
